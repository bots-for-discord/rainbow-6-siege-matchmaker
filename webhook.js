const model = require('./model')
const Discord = require('discord.js')

const send = (id, token, message) => {
    const hook = new Discord.WebhookClient(id, token)
    hook.send(message)
    delete hook
}

module.exports = {
    publish: (message) => {
        const subscibers = model.getAllWebhooks()
        if (subscibers.length) {
            subscibers.forEach(subsciber => {
                send(subsciber.id, subsciber.token, message)
            })
        }
    }
}

