const api = require('./api-ctrl')
const model = require('./model')

const Discord = require("discord.js")
const client = new Discord.Client()

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`)

    if(!model.init(process.env.DATABASE)) {
        console.error('Can not init databse')
        client.destroy()
    }
})

client.on("message", msg => {

    if (msg.channel.name !== process.env.BOT_CHANNEL.toLowerCase() ) { return }

    if (!msg.content.startsWith('/r6sm/')) { return }

    let command = String(msg.content).replace('/r6sm/', '')

    if (command.length === 0) {
        msg.reply(api.help)
        return
    }
    else {
        if (msg.member.roles.cache.find(role => role.name == process.env.USER_ROLE)) {
            if (!msg.author.bot) {
                msg.reply(api.proceed(command, {
                    tag: msg.author.tag,
                    name: msg.author.username
                }))
            }
            else {
                msg.reply('Go away messy bot !');
            }
        }
        else {
            msg.reply(`\n***Access denied** for user ${msg.author.tag}*.`);
        }
    }

})

client.login(process.env.BOT_TOKEN)