const Database = require('better-sqlite3')

let DB = ''

module.exports = {

    init: (path) => {
        DB = path
        try {
            const db = new Database(DB)
            db.exec(`CREATE TABLE IF NOT EXISTS matches (
                        id INTEGER PRIMARY KEY NOT NULL,
                        timestamp INTEGER NOT NULL,
                        creator TEXT NOT NULL,
                        players TEXT
                    );`)
            db.exec(`CREATE TABLE IF NOT EXISTS webhooks (
                        id TEXT NOT NULL,
                        token TEXT NOT NULL,
                        owner TEXT NOT NULL
                    );`)
            db.close()
            delete db
            return true

        } catch (error) {
            console.error(error)
            return false
        }

    },

    createNewMatch: (creator, timestamp) => {
        try {
            const db = new Database(DB)
            const stmt = db.prepare('INSERT INTO matches (id, timestamp, creator) VALUES(?, ?, ?)');
            const id = Date.now()
            stmt.run(id, timestamp, creator);
            db.close()
            delete db
            return id

        } catch (error) {
            console.error(error)
            return false
        }
    },

    removeMatch: (id, creator) => {
        try {
            const db = new Database(DB)
            let stmt = db.prepare('DELETE FROM matches WHERE id == ? AND creator == ?')
            stmt.run(id, creator);

            stmt = db.prepare('SELECT * FROM matches WHERE id == ? AND creator == ?')
            let info = stmt.all(id, creator);

            db.close()
            delete db

            if (!info.length) {
                return true
            }

        } catch (error) {
            console.error(error)
        }

        return false
    },

    getIncomingMatches: () => {
        try {
            const date = new Date()
            const db = new Database(DB)
            const stmt = db.prepare('SELECT id, timestamp FROM matches WHERE timestamp >= ?')
            const result = stmt.all(date.getTime())
            delete date
            db.close()
            delete db
            return result

        } catch (error) {
            console.error(error)
            return []
        }
    },

    getMatchDetails: (id) => {
        try {
            const db = new Database(DB)
            const stmt = db.prepare('SELECT * FROM matches WHERE id == ?')
            const result = stmt.all(id)
            db.close()
            delete db
            return result

        } catch (error) {
            console.error(error)
            return []
        }
    },

    addPlayer: (id, player) => {
        try {
            const db = new Database(DB)
            let stmt = db.prepare('SELECT players FROM matches WHERE id == ?')
            let data = stmt.all(id)

            if (data[0].players) {
                data = JSON.parse(data[0].players)
                data.push(player)
                data = JSON.stringify(data)
            }
            else {
                data = JSON.stringify([player])
            }

            stmt = db.prepare('UPDATE matches SET players = ? WHERE id == ?')
            data = stmt.run(data, id)

            db.close()
            delete db

            if (data.changes) {
                return true
            }

        } catch (error) {
            console.error(error)
        }

        return false
    },

    removePlayer: (id, player) => {
        try {
            const db = new Database(DB)
            let stmt = db.prepare('SELECT players FROM matches WHERE id == ?')
            let data = stmt.all(id)

            if (data[0].players) {
                data = JSON.parse(data[0].players)
                data.splice(data.indexOf(data.find(el => el === player)), 1)
                data = JSON.stringify(data)

                stmt = db.prepare('UPDATE matches SET players = ? WHERE id == ?')
                data = stmt.run(data, id)
            }

            db.close()
            delete db

            if (data.changes) {
                return true
            }

        } catch (error) {
            console.error(error)
        }

        return false
    },

    getPlayerParticipation: (player) => {
        try {
            const date = new Date()
            const db = new Database(DB)
            let stmt = db.prepare('SELECT * FROM matches WHERE timestamp >= ?')
            let data = stmt.all(date.getTime())
            delete date
            db.close()
            delete db

            if (data.length > 0) {
                let result = Array()

                data.forEach(row => {
                    let players = JSON.parse(row.players)
                    if (players) {
                        if (players.find(el => el === player)) {
                            result.push({
                                id: row.id,
                                timestamp: row.timestamp
                            })
                        }
                    }
                })

                return result
            }

        } catch (error) {
            console.error(error)
        }

        return []
    },

    addWebhook: (id, token, user) => {
        try {
            const db = new Database(DB)
            const stmt = db.prepare('INSERT INTO webhooks (id, token, owner) VALUES(?, ?, ?)')
            stmt.run(id, token, user)
            db.close()
            delete db
            return true

        } catch (error) {
            console.error(error)
            return false
        }
    },

    removeWebhook: (id, token, user) => {
        try {
            const db = new Database(DB)
            let stmt = db.prepare('DELETE FROM webhooks WHERE id == ? AND token == ? AND owner == ?')
            stmt.run(id, token, user);
            stmt = db.prepare('SELECT * FROM webhooks WHERE id == ? AND token == ? AND owner == ?')
            let info = stmt.all(id, user);
            db.close()
            delete db

            // if (!info.length) {
            return true
            // }

        } catch (error) {
            console.error(error)
        }

        return false
    },

    getAllWebhooks: () => {
        try {
            const db = new Database(DB)
            const stmt = db.prepare('SELECT * FROM webhooks')
            const result = stmt.all()
            delete date
            db.close()
            delete db
            return result

        } catch (error) {
            console.error(error)
            return []
        }
    }

} 