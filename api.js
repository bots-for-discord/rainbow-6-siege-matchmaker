const model = require('./model')
const webhooks = require('./webhook')

const STH_GH_MSG = `\nSomething goes wrong.\nTry again or contact with my dad.`

const formatedDateTime = (timestamp) => {
    let dt = new Date(timestamp)
    let sd = [
        dt.getUTCDate() < 10 ? `0${dt.getUTCDate()}` : dt.getUTCDate(),
        dt.getUTCMonth() < 9 ? `0${dt.getUTCMonth()+1}` : dt.getUTCMonth()+1,
        dt.getUTCFullYear()
    ]
    let st = [
        dt.getUTCHours() < 10 ? `0${dt.getUTCHours()}` : dt.getUTCHours(),
        dt.getUTCMinutes() < 10 ? `0${dt.getUTCMinutes()}` : dt.getUTCMinutes()
    ]

    delete dt
    return `${sd.join('-')} at ${st.join(':')}`
}

const matchDetailsTable = ({ timestamp, creator, players }) => {
    let table = `\`\`\`
    Time      |   ${formatedDateTime(timestamp)}
    ----------|----------------------------------------
    Initiator |   ${creator}
    ----------|----------------------------------------
    Players   |`

    if (players) {
        players = JSON.parse(players)
        table = table.concat(`   ${players[0]}`)
        players.forEach((player, i) => {
            if (i) {
                table = table.concat(`\n              |   ${player}`)
            }
        })
    }

    table = table.concat(`\`\`\``)

    return table
}

module.exports = {

    create: (params, user) => {
        if (params.length) {
            let dt = params[0].split('-')
            if (dt.length === 4) {
                dt = [
                    parseInt(dt[0]),
                    parseInt(dt[1]),
                    parseInt(dt[2]),
                    parseInt(dt[3]),
                ]
                if (!dt.includes(NaN)) {
                    const date = new Date()
                    const year = date.getFullYear()
                    const time = date.getTime()
                    delete date

                    dt = Date.UTC(year, dt[1]-1, dt[0], dt[2], dt[3])

                    if (dt > time) {
                        const id = model.createNewMatch(user.tag, dt)
                        if (id) {
                            webhooks.publish(`User \`${user.tag}\` has initiated a match \`${formatedDateTime(dt)}\`.\nDeclare your participation with the \`/r6sm/join/${id}\` command at \`${process.env.BOT_CHANNEL.toLowerCase()}\` channel.`)
                            return `OK`
                        }
                    }
                }
            }
        }

        return STH_GW_MSG
    },

    remove: (params, user) => {
        if (params.length) {
            if (model.removeMatch(parseInt(params[0]), user.tag)) {
                webhooks.publish(`User \`${user.tag}\` removed a match \`${parseInt(params[0])}\``)
                return `OK`
            }
        }

        return STH_GW_MSG
    },

    matches: () => {
        let data = model.getIncomingMatches()
        if (data.length) {
            let response = String()
            data.forEach(m => {
                response = response.concat(`\n- :gun: \`${formatedDateTime(m.timestamp)}\` *(id: ${m.id})*`)
            });
            return response
        }
        else {
            return `\nNo matches scheduled.`
        }

    },

    match: (params) => {
        if (params.length) {
            let data = model.getMatchDetails(parseInt(params[0]));
            if (data.length) {
                data = data[0]
                let response = String()
                response = response.concat(`\n:gun:  \`Match details:\` *${data.id}*`)
                response = response.concat(`\n${matchDetailsTable(data)}`)
                return response
            }
            return `\nMatch with this id does not exist.`
        }
        return `\nYou need to specify match id to read the details.`
    },

    join: (params, user) => {
        if (params.length) {
            if (model.addPlayer(parseInt(params[0]), user.tag)) {
                webhooks.publish(`User \`${user.tag}\` joined the match \`${parseInt(params[0])}\``)
                return `OK`
            }
        }

        return STH_GW_MSG
    },

    "give-up": (params, user) => {
        if (params.length) {
            if (model.removePlayer(parseInt(params[0]), user.tag)) {
                webhooks.publish(`User \`${user.tag}\` gave up the match \`${parseInt(params[0])}\` (looser!)`)
                return `OK`
            }
        }

        return STH_GW_MSG
    },

    participation: (params, user) => {
        if (params.length) {
            return `N/A`
        }
        else {
            let data = model.getPlayerParticipation(user.tag)
            if (data.length) {
                let response = `\nYou have declared participation in these matches:`
                data.forEach(m => {
                    response = response.concat(`\n- :gun: \`${formatedDateTime(m.timestamp)}\` *(id: ${m.id})*`)
                });
                return response
            }
            else {
                return `You have not declared participation in any matches.`
            }
        }
    },

    "source-code": () => {
        return `
        Yes! I'm open source project (at MIT license).
        https://gitlab.com/bots-for-discord/rainbow-6-siege-matchmaker`
    },

    webhook: (params, user) => {
        if (params.length > 0 && params.length < 4) {

            const wh_commands = {

                list: () => {
                    const data = model.getAllWebhooks()
                    if (data.length) {
                        let result = '\nRegistred webhooks:\n```'
                        data.forEach((wh, i) => {
                            result = result.concat(
                                `\n(${i + 1}) ${wh.owner} => ${wh.id} => ${wh.token}`
                            )
                        })
                        return result.concat('\n```')
                    }
                    else {
                        console.log(2)
                        return 'No registered webhooks.'
                    }
                },

                add: (id, token, user) => {
                    if( !id || !token || !user ) {
                        return STH_GW_MSG
                    }

                    if (model.addWebhook(id, token, user)) {
                        return `OK`
                    }
                    return STH_GW_MSG
                },

                remove: (id, token, user) => {
                    if( !id || !token || !user ) {
                        return STH_GW_MSG
                    }

                    if (model.removeWebhook(id, token, user)) {
                        return `OK`
                    }
                    return STH_GW_MSG
                }
            }

            if (params[0] in wh_commands) {
                return wh_commands[params[0]](params[1], params[2], user.tag)
            }
        }

        return STH_GW_MSG
    }
}