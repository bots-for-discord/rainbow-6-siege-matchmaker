const api = require('./api')

const help = {
    short: ` :gun: Try to use the \`/r6sm/help\` command.`,
    full: `
    :gun: Rainbow 6 Siege Matchmaker Bot
    
    > \`/r6sm/create/DD-MM-HH-mm\`
    > create new match at given day (DD), month (MM), hour (HH) and minutes (mm)
    
    > \`/r6sm/matches\`
    > display incoming matches
    
    > \`/r6sm/match/id\`
    > display details of the match with given id
    
    > \`/r6sm/remove/id\`
    > cancel the match with given id
    
    > \`/r6sm/join/id\`
    > declare your participation in the match with the given id
    
    > \`/r6sm/give-up/id\`
    > declare your resignation from participation in the match with given id
    
    > \`/r6sm/participation\`
    > display matches you want to take part in
    
    > \`/r6sm/webhook/add/id/token\`
    > add your webhook (with given id and token) for receive notifications
    
    > \`/r6sm/webhook/remove/id/token\`
    > remove your webhook from database
    
    > \`/r6sm/webhook/list\`
    > displays all registred webhooks
    
    > \`/r6sm/source-code\`
    > display a hyperlink to my source`
}

const proceed = (command, member) => {
    if(command === 'help') { return help.full }
    let params = command.split('/');
    let cmd = params.splice(0,1)
    if(cmd in api) { return api[cmd](params, member) }
    else { return help.short }
}

module.exports = {
    help: help.short,
    proceed: proceed
}