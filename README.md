# Rainbow 6 Siege Matchmaker Bot

## Instalation

1. Install dependencies with `npm install`.
2. Setup envoirmet variables or use `.env` file:
    - `BOT_TOKEN` - your bot token
    - `BOT_CHANNEL` - the bot will only accept messages on this channel
    - `USER_ROLE` - the bot will only accept commands from users with this role
    - `DATABASE` - path to your database file

## Run

You can run bot with one of these commands:
- `node -r dotenv/config index.js dotenv_config_path=./.env` if you are using the `.env` file
- `node index.js` if you have defined envoirment variables


## Usage

> `/r6sm/help`
> show help
    
> `/r6sm/create/DD-MM-HH-mm`
> create new match at given day (DD), month (MM), hour (HH) and minutes (mm)
    
> `/r6sm/matches`
> show incoming matches

> `/r6sm/match/id`
> show details of the match with given id

> `/r6sm/remove/id`
> cancel the match with given id

> `/r6sm/join/id`
> declare your participation in the match with the given id

> `/r6sm/give-up/id`
> declare your resignation from participation in the match with given id

> `/r6sm/participation`
> show matches you want to take part in

> `/r6sm/webhook/add/id/token`
> add your webhook (with given id and token) for receive notifications

> `/r6sm/webhook/remove/id/token`
> remove your webhook from database

> `/r6sm/webhook/list`
> show all registred webhooks

> `/r6sm/source-code`
> show a hyperlink to my source
